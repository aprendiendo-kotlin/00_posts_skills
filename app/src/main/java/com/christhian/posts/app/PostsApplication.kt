package com.christhian.posts.app

import android.app.Application
import com.facebook.stetho.Stetho
import timber.log.Timber

class PostsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        Timber.plant(Timber.DebugTree())
    }

}