package com.christhian.posts.ui.fragments.posts

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.christhian.posts.R
import com.christhian.posts.databinding.FragmentPostsBinding
import com.christhian.posts.extensions.showSnackBar
import com.christhian.posts.ui.fragments.posts.adapter.PostsAdapter
import com.christhian.posts.ui.fragments.posts.adapter.PostsItemClickListener
import com.christhian.posts.utilities.viewmodel.PostsViewModelFactory

/**
 * A simple [Fragment] subclass.
 */
class PostsFragment : Fragment() {

    private lateinit var binding: FragmentPostsBinding

    private lateinit var viewModelFactory: PostsViewModelFactory

    private val viewModel: PostsViewModel by viewModels{
        viewModelFactory
    }

    private lateinit var postsAdapter: PostsAdapter

    private lateinit var application: Application

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //Gets context of application from activity
        application = requireNotNull(value = this.activity).application

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_posts,
            container,
            false
        )

        setupViewModel()

        setupAdapter()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setupViewModel() {

        //Obtencion de viewmodel atraves del patron factory
        viewModelFactory = PostsViewModelFactory(
            application
        )

        // Set the viewmodel for databinding - this allows the bound layout access to all of the
        // data in the VieWModel
        binding.viewModel = viewModel

        viewModel.getPosts()

    }

    private fun setupAdapter() {

        //Set the adapter with the class listener
        postsAdapter = PostsAdapter(PostsItemClickListener {
            this.showSnackBar(binding.coordinatorPost, it.id.toString())
            this.findNavController().navigate(
                PostsFragmentDirections.actionPostsFragmentToDetailPostFragment(it)
            )
        })

        //Set the adapter in recyclerview
        binding.postsRecycler.adapter = postsAdapter

    }

}