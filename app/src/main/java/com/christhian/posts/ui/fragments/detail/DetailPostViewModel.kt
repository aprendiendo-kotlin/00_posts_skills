package com.christhian.posts.ui.fragments.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.christhian.posts.objects.Post

class DetailPostViewModel : ViewModel() {

    //Propierty testString
    private val _post = MutableLiveData<Post>()

    val post: LiveData<Post>
        get() = _post

    init {
    }

    fun setPostValue(value: Post) {
        _post.value = value
    }

}