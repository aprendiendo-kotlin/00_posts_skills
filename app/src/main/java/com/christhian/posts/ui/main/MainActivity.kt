package com.christhian.posts.ui.main

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.navOptions
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.christhian.posts.R
import com.christhian.posts.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener,
    BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMainBinding

    private lateinit var navController: NavController

    private lateinit var appBarConfiguration: AppBarConfiguration

    private val navOptions by lazy {
        navOptions {
            anim {
                enter = R.anim.fade_in
                exit = R.anim.fade_out
                popEnter = R.anim.fade_in
                popExit = R.anim.fade_out
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        //Set the Activity's content view to the given layout and return the associated binding.
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )

        //Find a NavController given the id of a View and its containing
        navController = findNavController(R.id.nav_host_fragment)

        //Set a Toolbar to act as the ActionBar for this Activity window.
        setSupportActionBar(binding.toolbar)

        //Configuration options for NavigationUI methods that interact with implementations of the
        // app bar
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.postsFragment,
                R.id.favoritesFragment
            ), binding.drawerLayout
        )

        //Sets up a Toolbar for use with a NavController
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration)

        //Sets up a Listeners for navigation from differents elements from UI
        binding.navigationView.setNavigationItemSelectedListener(this)

        binding.bottomNavView.setOnNavigationItemSelectedListener(this)

        setOnDestinationChangedListener()
    }

    /**
     * Implementa un escuchador a la navegacion y altera la UI de acuerdo al fragmento usado
     */
    private fun setOnDestinationChangedListener() {

        navController.addOnDestinationChangedListener { _, destination, _ ->

            when (destination.id) {

                R.id.aboutFragment,
                R.id.whatFragment,
                R.id.whoFragment,
                R.id.detailPostFragment -> {
                    binding.bottomNavView.visibility = View.GONE
                    binding.toolbar.visibility = View.VISIBLE
                }

                else -> {
                    binding.bottomNavView.visibility = View.VISIBLE
                    binding.toolbar.visibility = View.VISIBLE
                }

            }

        }

    }


    /**
     * Escuchador del item seleccionado para la navegacion
     */
    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {

        binding.drawerLayout.closeDrawers()

        when (menuItem.itemId) {

            //Items from BottomNavigationView
            R.id.postsItem -> navController.navigate(
                R.id.postsFragment,
                null,
                navOptions
            )
            R.id.favoritesItem -> navController.navigate(
                R.id.favoritesFragment,
                null,
                navOptions
            )

            //Items from NavigationView
            R.id.aboutItem -> navController.navigate(
                R.id.aboutFragment,
                null,
                navOptions
            )
            R.id.whoItem -> navController.navigate(
                R.id.whoFragment,
                null,
                navOptions
            )
            R.id.whatItem -> navController.navigate(
                R.id.whatFragment,
                null,
                navOptions
            )

        }

        return true
    }

}
