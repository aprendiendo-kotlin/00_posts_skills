package com.christhian.posts.ui.fragments.what

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.christhian.posts.R
import com.christhian.posts.databinding.FragmentWhatBinding

/**
 * A simple [Fragment] subclass.
 */
class WhatFragment : Fragment() {

    private lateinit var binding: FragmentWhatBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_what,
            container,
            false
        )

        // Inflate the layout for this fragment
        return binding.root
    }

}
