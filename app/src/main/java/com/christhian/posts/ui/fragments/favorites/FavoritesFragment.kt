package com.christhian.posts.ui.fragments.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.christhian.posts.R
import com.christhian.posts.databinding.FragmentFavoritesBinding

/**
 * A simple [Fragment] subclass.
 */
class FavoritesFragment : Fragment() {

    private lateinit var binding: FragmentFavoritesBinding

    private lateinit var viewModel: FavoritesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_favorites,
            container,
            false
        )

        viewModel = ViewModelProvider(this)
            .get(FavoritesViewModel::class.java)

        binding.viewModel = viewModel

        viewModel.setTestString(getString(R.string.text_favorites))

        // Inflate the layout for this fragment
        return binding.root
    }

}
