package com.christhian.posts.ui.fragments.posts.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.christhian.posts.objects.Post

class PostsAdapter(private val postsItemClickListener: PostsItemClickListener) :
    ListAdapter<Post, PostsViewHolder>(PostsDiffCallback()) {

    /**
     * @param postsViewHolder PostViewHolder
     * @param position Int
     */
    override fun onBindViewHolder(postsViewHolder: PostsViewHolder, position: Int) {
        val item = getItem(position)
        postsViewHolder.bind(postsItemClickListener, item)
    }

    /**
     * @param parent ViewGroup
     * @param viewType Int
     * @return PostViewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        return PostsViewHolder.from(
            parent
        )
    }

}

