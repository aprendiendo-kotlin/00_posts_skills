package com.christhian.posts.ui.fragments.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.christhian.posts.R
import com.christhian.posts.databinding.FragmentDetailPostBinding

/**
 * A simple [Fragment] subclass.
 */
class DetailPostFragment : Fragment() {

    private lateinit var binding: FragmentDetailPostBinding

    private lateinit var viewModel: DetailPostViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_detail_post,
            container,
            false
        )

        viewModel = ViewModelProvider(this)
            .get(DetailPostViewModel::class.java)

        binding.viewModel = viewModel

        val args = DetailPostFragmentArgs.fromBundle(requireArguments())


        viewModel.setPostValue(args.post)

        // Inflate the layout for this fragment
        return binding.root
    }

}
