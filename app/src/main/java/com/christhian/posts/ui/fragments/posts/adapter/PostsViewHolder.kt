package com.christhian.posts.ui.fragments.posts.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.christhian.posts.databinding.ItemPostBinding
import com.christhian.posts.objects.Post

/**
 * @property binding ListItemPostBinding
 * @constructor
 */
class PostsViewHolder private constructor(val binding: ItemPostBinding) :
    RecyclerView.ViewHolder(binding.root) {
    /**
     * @param postsItemClickListener PostItemListener
     * @param item Post
     */
    fun bind(postsItemClickListener: PostsItemClickListener, item: Post) {
        binding.post = item
        binding.clickListener = postsItemClickListener
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): PostsViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemPostBinding.inflate(layoutInflater, parent, false)

            return PostsViewHolder(binding)
        }
    }
}