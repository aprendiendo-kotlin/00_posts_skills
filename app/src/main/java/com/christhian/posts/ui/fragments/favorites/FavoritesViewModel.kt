package com.christhian.posts.ui.fragments.favorites

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FavoritesViewModel : ViewModel() {

    //Propierty testString
    private val _testString = MutableLiveData<String>()

    val testString: LiveData<String>
        get() = _testString

    init {
        _testString.value = ""
    }

    fun setTestString(value: String) {
        _testString.value = value
    }

}