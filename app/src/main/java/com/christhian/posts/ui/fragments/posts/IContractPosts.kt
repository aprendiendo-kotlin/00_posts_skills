package com.christhian.posts.ui.fragments.posts

import com.christhian.posts.dto.posts.PostsResDTO

interface IContractPosts {

    interface ViewModel {
        fun getPosts()
    }

    interface Repository {
        suspend fun getPosts(): MutableList<PostsResDTO>?
    }

}