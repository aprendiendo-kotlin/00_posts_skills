package com.christhian.posts.ui.fragments.posts

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.christhian.posts.dto.posts.PostsResDTO
import com.christhian.posts.objects.Post
import com.christhian.posts.repository.PostsRepository
import com.christhian.posts.utilities.viewmodel.BaseViewModel
import kotlinx.coroutines.launch
import timber.log.Timber

class PostsViewModel(application: Application) :
    BaseViewModel(application),
    IContractPosts.ViewModel {

    private val _listPost = MutableLiveData<List<Post>>()
    val listPost: LiveData<List<Post>>
        get() = _listPost

    private val repository = PostsRepository()

    init {

    }

    override fun getPosts() {
        viewModelScope.launch {
            try {
                repository.getPosts()
            } catch (ex: Exception) {
                Timber.e(ex)
            }
        }
    }

}