package com.christhian.posts.ui.fragments.posts.adapter

import androidx.recyclerview.widget.DiffUtil
import com.christhian.posts.objects.Post


/** Callback for calculating the diff between two non-null items in a list.
 *
 * Used by ListAdapter to calculate the minumum number of changes between and old list and a new
 * list that's been passed to `submitList`.
 */
class PostsDiffCallback : DiffUtil.ItemCallback<Post>() {
    /**
     * @param oldItem Post
     * @param newItem Post
     * @return Boolean
     */
    override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem.id == newItem.id
    }

    /**
     * @param oldItem Post
     * @param newItem Post
     * @return Boolean
     */
    override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem == newItem
    }
}