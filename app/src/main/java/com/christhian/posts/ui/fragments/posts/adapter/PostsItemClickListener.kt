package com.christhian.posts.ui.fragments.posts.adapter

import com.christhian.posts.objects.Post

/**
 * @property clickListener Function1<[@kotlin.ParameterName] Post, Unit>
 * @constructor
 */
class PostsItemClickListener(val clickListener: (post: Post) -> Unit) {

    /**
     * @param post Post
     */
    fun onClick(post: Post) = clickListener(post)

}
