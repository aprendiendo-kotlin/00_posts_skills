package com.christhian.posts.network

import com.christhian.posts.dto.posts.PostsResDTO
import retrofit2.Response
import retrofit2.http.GET

interface PostsAPI {

    /**
     *
     */
    @GET("/posts")
    suspend fun getPosts(): Response<List<PostsResDTO>>

}