package com.christhian.posts.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

enum class NetworkApiStatus { LOADING, ERROR, DONE }

const val URL = "https://jsonplaceholder.typicode.com/"

/** Build the Moshi object that Retrofit will be using, making sure to add the Kotlin adapter for
 * full Kotlin compatibility.
 */
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()


private val httpClient = OkHttpClient.Builder()
    .readTimeout(15, TimeUnit.MINUTES)
    .connectTimeout(15, TimeUnit.MINUTES)
    .build()

/** Main entry point for network access. Call like `Network.devPosts.getPlaylist()`
 */
object PostsRetrofit {

    // Configure retrofit to parse JSON and use coroutines
    private val retrofit = Retrofit.Builder()
        .baseUrl(URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .client(httpClient)
        .build()

    val postsApi: PostsAPI = retrofit.create(PostsAPI::class.java)
}