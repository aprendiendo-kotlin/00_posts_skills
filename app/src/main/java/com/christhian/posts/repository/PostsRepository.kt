package com.christhian.posts.repository


import com.christhian.posts.dto.posts.PostsResDTO
import com.christhian.posts.network.PostsRetrofit
import com.christhian.posts.ui.fragments.posts.IContractPosts
import timber.log.Timber

class PostsRepository() : IContractPosts.Repository {
    /**
     * @return LiveData<List<Post>>
     */
    override suspend fun getPosts(): MutableList<PostsResDTO>? {

        val response = PostsRetrofit.postsApi.getPosts()

        return if (response.isSuccessful) {

            val postList: MutableList<PostsResDTO> = response.body()!!.toMutableList()

            if (postList.isNotEmpty()) {

                for (post in postList)
                    Timber.i(post.title)

                postList

            } else {

                null

            }

        } else {

            null

        }

    }
}