package com.christhian.posts.extensions

import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

fun Fragment.showSnackBar(coordinator: CoordinatorLayout, value: String) {
    Snackbar.make(coordinator, value, Snackbar.LENGTH_SHORT).show()
}