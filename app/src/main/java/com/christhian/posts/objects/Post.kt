package com.christhian.posts.objects

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * @property body String
 * @property id Int
 * @property title String
 * @property userId Int
 * @property read Boolean
 * @property favorite Boolean
 * @constructor
 */
@Parcelize
data class Post(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String,
    var read: Boolean = false,
    var favorite: Boolean = false
) : Parcelable