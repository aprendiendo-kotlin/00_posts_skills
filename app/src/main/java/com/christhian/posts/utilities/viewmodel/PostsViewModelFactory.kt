package com.christhian.posts.utilities.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.christhian.posts.ui.fragments.posts.PostsViewModel

class PostsViewModelFactory(
    private val application: Application
) : ViewModelProvider.Factory {

    /**
     * @param modelClass Class<T>
     * @return T
     */
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(PostsViewModel::class.java))

            return PostsViewModel(application) as T

        throw IllegalArgumentException("Unknown ViewModel class")
    }

}