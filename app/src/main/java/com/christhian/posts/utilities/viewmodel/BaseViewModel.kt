package com.christhian.posts.utilities.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import kotlinx.coroutines.*

/**
 * @property viewModelJob Job
 * @property uiScope CoroutineScope
 * @constructor
 */
open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    private var viewModelJob = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     */
    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        uiScope.cancel()
    }

}